///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string newLocation, enum Color newColor, enum Gender newGender ) {
   gender = newGender;              /// Get from the constructor... not all aku are the same gender (this is a has-a relationship)
   species = "Loxioides bailleui";  /// An is-a relationship. All aku  are the same species.
   featherColor = newColor;         /// A has-a relationship, so it comes through the constructor
   foundAt = newLocation;           /// A has-a relationship.

   isMigratory = false;             /// An is-a relationship.
}


const string Palila::speak() {
   return string( "Tweet" );
}


/// Print our Palila and where it was found first... then print whatever information Fish holds.
void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << foundAt << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm
