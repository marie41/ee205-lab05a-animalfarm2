///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Nene : public Bird {
public:
   string tagID;

   Nene( string newID, enum Color newColor, enum Gender newGender );

   virtual const string speak();

   void printInfo();
};

} // namespace animalfarm
