///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string newID, enum Color newColor, enum Gender newGender ) {
   gender = newGender;              /// Get from the constructor... not all aku are the same gender (this is a has-a relationship)
   species = "Branta sandvicensis"; /// An is-a relationship. All aku  are the same species.
   featherColor = newColor;         /// A has-a relationship, so it comes through the constructor
   tagID = newID;                   /// A has-a relationship.

   isMigratory = true;              /// An is-a relationship.
}


const string Nene::speak() {
   return string( "Nay, nay" );
}


/// Print our Nene and where it's tag id first... then print whatever information Fish holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm
