///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Nunu : public Fish {
public:
   bool isNative;

   Nunu( bool newNative, enum Color newColor, enum Gender newGender );

   void printInfo();
};

} // namespace animalfarm
